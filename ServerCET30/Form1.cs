﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace ServerCET30
{
    public partial class Form1 : Form
    {
        TcpListener mTcpListener; //Escuta num IP/Port
        TcpClient mTpcClient; // classe que liga, recebe, envia dados pela rede


        public Form1()
        {
            InitializeComponent();
        }

        private void bttnListening_Click(object sender, EventArgs e)
        {
            IPAddress ipAdress; //Guarda IP do Server
            int nPort; //Guarda porto do server

            if (!IPAddress.TryParse(txtIP.Text,out ipAdress))
            {
                txtMsg.Text += "Endereço IP é inválico" + Environment.NewLine;
                return;
            }

            if (!int.TryParse(txtPort.Text, out nPort))

            {
                txtMsg.Text += "Port inválido" + Environment.NewLine;
                return;
            }

            if (nPort < 1024 || nPort > 65535)

            {
                txtMsg.Text += "Port inválido" + Environment.NewLine;
                return;
            }

            mTcpListener = new TcpListener(ipAdress, nPort);


            mTcpListener.Start();
            txtMsg.Text += "Servidor à escuta no IP " + ipAdress + "e no port" + nPort;
            mTcpListener.BeginAcceptTcpClient(onCompleteAcceptClient, mTcpListener);

            // netstat -an |find /i "listening" VERIFICAR SE O PROGRAMA FUNCIONA E O IP ESTÁ à ESCUTA
        }

        private void onCompleteAcceptClient(IAsyncResult iar)
        {
            TcpListener tcpl = (TcpListener)iar.AsyncState;
            mTpcClient = tcpl.EndAcceptTcpClient(iar);
            txtMsg.Invoke(new Action<string>(doInvoke), 
                "Cliente aceite..." + "Cliente IP" + ((IPEndPoint)mTpcClient.Client.RemoteEndPoint).Address + 
                "Port:" + ((IPEndPoint)mTpcClient.Client.RemoteEndPoint).Port);




        }

        private void doInvoke(string str)
        {
            txtMsg.Text += str + Environment.NewLine;
        }
    }
}
